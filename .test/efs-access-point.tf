################################################################################
# Modules
################################################################################

module "efs_access_point" {
  source           = "../efs/access-point"
  owner            = local.owner
  company          = local.company
  file_system_id   = module.efs_file_system.id
  file_system_name = module.efs_file_system.name
}

################################################################################
