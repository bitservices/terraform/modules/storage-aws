################################################################################

module "efs_file_system" {
  source  = "../efs/file-system"
  vpc     = local.vpc
  name    = local.class
  owner   = local.owner
  company = local.company
}

################################################################################
