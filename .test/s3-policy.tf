################################################################################
# Locals
################################################################################

locals {
  s3_policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Example permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::012345678901:user/bucket_user"
            },
            "Action": "s3:*",
            "Resource": [
                "${module.s3_bucket.arn}",
                "${module.s3_bucket.arn}/*"
            ]
        }
    ]
}
POLICY
}

################################################################################
# Modules
################################################################################

module "s3_policy" {
  source = "../s3/policy"
  bucket = module.s3_bucket.name
  policy = local.s3_policy
}

################################################################################
