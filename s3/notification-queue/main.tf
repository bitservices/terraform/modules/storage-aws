################################################################################
# Required Variables
################################################################################

variable "bucket" {
  type        = string
  description = "Name of the S3 bucket that will send notifications."
}

variable "queue" {
  type        = string
  description = "ARN of the SQS queue that will receive the notifications."
}

################################################################################
# Optional Variables
################################################################################

variable "name" {
  type        = string
  default     = null
  description = "Specifies unique identifier for the notification configuration."
}

################################################################################

variable "events" {
  type        = list(string)
  default     = ["s3:ObjectCreated:*"]
  description = "List of specific events for which to send notifications."
}

################################################################################

variable "object_prefix" {
  type        = string
  default     = ""
  description = "Specifies object key name prefix."
}

variable "object_suffix" {
  type        = string
  default     = ""
  description = "Specifies object key name suffix."
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_notification" "scope" {
  bucket = var.bucket

  queue {
    id            = var.name
    queue_arn     = var.queue
    events        = var.events
    filter_prefix = var.object_prefix
    filter_suffix = var.object_suffix
  }
}

################################################################################
# Outputs
################################################################################

output "bucket" {
  value = var.bucket
}

output "queue" {
  value = var.queue
}

################################################################################

output "events" {
  value = var.events
}

################################################################################

output "object_prefix" {
  value = var.object_prefix
}

output "object_suffix" {
  value = var.object_suffix
}

################################################################################

output "name" {
  value = aws_s3_bucket_notification.scope.id
}

################################################################################
