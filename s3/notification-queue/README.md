<!----------------------------------------------------------------------------->

# s3/notification-queue

#### Manage [S3] bucket notifications that are put into [SQS] queues

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/storage/aws//s3/notification-queue`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "class"   { default = "foobar"                   }
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }


module "my_s3_bucket" {
  source  = "gitlab.com/bitservices/storage/aws//s3/bucket"
  class   = var.class
  owner   = var.owner
  company = var.company
}


module "my_sqs_queue" {
  source       = "gitlab.com/bitservices/queue/aws//sqs/queue"
  class        = var.class
  owner        = var.owner
  company      = var.company
  queue_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": "*",
      "Action": "sqs:SendMessage",
      "Resource": "*",
      "Condition": {
        "ArnEquals": { "aws:SourceArn": "${module.my_s3_bucket.arn}" }
      }
    }
  ]
}
POLICY
}

module "my_s3_notification_queue" {
  source = "gitlab.com/bitservices/storage/aws//s3/notification-queue"
  bucket = module.my_s3_bucket.name
  queue  = module.my_sqs_queue.arn
}
```

<!----------------------------------------------------------------------------->

[S3]:  https://aws.amazon.com/s3
[SQS]: https://aws.amazon.com/sqs/

<!----------------------------------------------------------------------------->
