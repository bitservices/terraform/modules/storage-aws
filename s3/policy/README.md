<!----------------------------------------------------------------------------->

# s3/policy

#### Manage [S3] bucket policies

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/storage/aws//s3/policy`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_s3_bucket" {
  source  = "gitlab.com/bitservices/storage/aws//s3/bucket"
  owner   = "terraform@bitservices.io"
  class   = "foobar"
  company = "BITServices Ltd"
}

module "my_s3_policy" {
  source = "gitlab.com/bitservices/storage/aws//s3/policy"
  bucket = module.my_s3_bucket.name
  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Example permissions",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::012345678901:user/bucket_user"
            },
            "Action": "s3:*",
            "Resource": [
                "${module.my_s3_bucket.arn}",
                "${module.my_s3_bucket.arn}/*"
            ]
        }
    ]
}
POLICY
}
```

<!----------------------------------------------------------------------------->

[S3]: https://aws.amazon.com/s3

<!----------------------------------------------------------------------------->
