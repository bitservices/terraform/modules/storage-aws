################################################################################
# Required Variables
################################################################################

variable "bucket" {
  type        = string
  description = "The full name of the S3 bucket to apply this policy to."
}

variable "policy" {
  type        = string
  description = "The policy JSON to apply to the S3 bucket."
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_policy" "scope" {
  bucket = var.bucket
  policy = var.policy
}

################################################################################
# Outputs
################################################################################

output "bucket" {
  value = var.bucket
}

output "policy" {
  value = var.policy
}

################################################################################
