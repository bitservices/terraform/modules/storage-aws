<!----------------------------------------------------------------------------->

# s3/bucket

#### Manage [S3] buckets and websites

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/storage/aws//s3/bucket`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_s3_bucket" {
  source  = "gitlab.com/bitservices/storage/aws//s3/bucket"
  class   = "foobar"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[S3]: https://aws.amazon.com/s3/

<!----------------------------------------------------------------------------->
