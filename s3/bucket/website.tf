################################################################################
# Optional Variables
################################################################################

variable "website_enabled" {
  type        = bool
  default     = false
  description = "Should this S3 bucket act as a static content website?"
}

variable "website_index_document" {
  type        = string
  default     = "index.html"
  description = "Amazon S3 returns this index document when requests are made to the root domain or any of the subfolders."
}

variable "website_error_document" {
  type        = string
  default     = null
  description = "An absolute path to the document to return in case of a 4XX error."
}

################################################################################

variable "website_redirect_all_host" {
  type        = string
  default     = null
  description = "A hostname to redirect all website requests for this bucket to. Redirecting all website requests is disabled if not specified."
}

variable "website_redirect_all_protocol" {
  type        = string
  default     = null
  description = "Protocol to use when redirecting all website requests. The default is the protocol that is used in the original request."
}

################################################################################
# Locals
################################################################################

locals {
  website_index_document = var.website_redirect_all_host == null ? var.website_index_document : null
  website_error_document = var.website_redirect_all_host == null ? var.website_error_document : null
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_website_configuration" "scope" {
  count  = var.website_enabled ? 1 : 0
  bucket = aws_s3_bucket.scope.bucket

  dynamic "index_document" {
    for_each = compact([local.website_index_document])

    content {
      suffix = index_document.value
    }
  }

  dynamic "error_document" {
    for_each = compact([local.website_error_document])

    content {
      key = error_document.value
    }
  }

  dynamic "redirect_all_requests_to" {
    for_each = var.website_redirect_all_host != null ? { (var.website_redirect_all_host) = var.website_redirect_all_protocol } : {}

    content {
      protocol  = redirect_all_requests_to.value
      host_name = redirect_all_requests_to.key
    }
  }
}

################################################################################
# Outputs
################################################################################

output "website_index_document" {
  value = local.website_index_document
}

output "website_error_document" {
  value = local.website_error_document
}

################################################################################

output "website_redirect_all_host" {
  value = var.website_redirect_all_host
}

output "website_redirect_all_protocol" {
  value = var.website_redirect_all_protocol
}

################################################################################

output "website_id" {
  value = length(aws_s3_bucket_website_configuration.scope) == 1 ? aws_s3_bucket_website_configuration.scope[0].id : null
}

output "website_domain" {
  value = length(aws_s3_bucket_website_configuration.scope) == 1 ? aws_s3_bucket_website_configuration.scope[0].website_domain : null
}

output "website_endpoint" {
  value = length(aws_s3_bucket_website_configuration.scope) == 1 ? aws_s3_bucket_website_configuration.scope[0].website_endpoint : null
}

################################################################################
