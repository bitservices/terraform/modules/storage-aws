################################################################################
# Optional Variables
################################################################################

variable "lifecycle_expire_delete_markers" {
  type        = bool
  default     = true
  description = "Set this to 'true' to automatically cleanup delete markers for expired objects. Only relevant if 'lifecycle_expire_objects' is 'false'."
}

variable "lifecycle_expire_delete_markers_name" {
  type        = string
  default     = "expire-delete-markers"
  description = "Identifier for the expire delete markers rule."
}

variable "lifecycle_expire_delete_markers_prefix" {
  type        = string
  default     = ""
  description = "Object key prefix identifying one or more objects to which the cleanup delete markers rule applies. Blank for whole bucket."
}

################################################################################

variable "lifecycle_expire_objects" {
  type        = bool
  default     = false
  description = "Expire objects after a period of time."
}

variable "lifecycle_expire_objects_days" {
  type        = number
  default     = 365
  description = "After how many days should objects be expired."
}

variable "lifecycle_expire_objects_name" {
  type        = string
  default     = "expire-objects"
  description = "Identifier for the expire objects rule."
}

variable "lifecycle_expire_objects_prefix" {
  type        = string
  default     = ""
  description = "Object key prefix identifying one or more objects to which the cleanup objects rule applies. Blank for whole bucket."
}

################################################################################

variable "lifecycle_expire_versions" {
  type        = bool
  default     = false
  description = "Expire old versions of objects after a period of time."
}

variable "lifecycle_expire_versions_days" {
  type        = number
  default     = 31
  description = "After how many days should old versions of an object be expired."
}

variable "lifecycle_expire_versions_name" {
  type        = string
  default     = "expire-versions"
  description = "Identifier for the expire verions rule."
}

variable "lifecycle_expire_versions_prefix" {
  type        = string
  default     = ""
  description = "Object key prefix identifying one or more objects to which the cleanup old versions rule applies. Blank for whole bucket."
}

################################################################################

variable "lifecycle_multipart_upload_clear" {
  type        = bool
  default     = true
  description = "Specifies multipart upload clear rule status."
}

variable "lifecycle_multipart_upload_clear_days" {
  type        = number
  default     = 7
  description = "Specifies the number of days after initiating a multipart upload when the multipart upload must be completed."
}

variable "lifecycle_multipart_upload_clear_name" {
  type        = string
  default     = "multipart-upload-clear"
  description = "Identifier for the multipart upload clear rule."
}

variable "lifecycle_multipart_upload_clear_prefix" {
  type        = string
  default     = ""
  description = "Object key prefix identifying one or more objects to which the multipart upload clear rule applies. Blank for whole bucket."
}

################################################################################
# Locals
################################################################################

locals {
  lifecycle_expire_delete_markers = var.lifecycle_expire_objects ? false : var.lifecycle_expire_delete_markers
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_lifecycle_configuration" "scope" {
  bucket = aws_s3_bucket.scope.bucket

  rule {
    id     = var.lifecycle_multipart_upload_clear_name
    status = var.lifecycle_multipart_upload_clear ? "Enabled" : "Disabled"

    abort_incomplete_multipart_upload {
      days_after_initiation = var.lifecycle_multipart_upload_clear_days
    }

    filter {
      prefix = var.lifecycle_multipart_upload_clear_prefix
    }
  }

  rule {
    id     = var.lifecycle_expire_delete_markers_name
    status = local.lifecycle_expire_delete_markers ? "Enabled" : "Disabled"

    expiration {
      expired_object_delete_marker = var.lifecycle_expire_delete_markers
    }

    filter {
      prefix = var.lifecycle_expire_delete_markers_prefix
    }
  }

  rule {
    id     = var.lifecycle_expire_objects_name
    status = var.lifecycle_expire_objects ? "Enabled" : "Disabled"

    expiration {
      days = var.lifecycle_expire_objects_days
    }

    filter {
      prefix = var.lifecycle_expire_objects_prefix
    }
  }

  rule {
    id     = var.lifecycle_expire_versions_name
    status = var.lifecycle_expire_versions ? "Enabled" : "Disabled"

    filter {
      prefix = var.lifecycle_expire_versions_prefix
    }

    noncurrent_version_expiration {
      noncurrent_days = var.lifecycle_expire_versions_days
    }
  }

  depends_on = [
    aws_s3_bucket_versioning.scope
  ]
}

################################################################################
# Outputs
################################################################################

output "lifecycle_expire_delete_markers" {
  value = local.lifecycle_expire_delete_markers
}

output "lifecycle_expire_delete_markers_name" {
  value = var.lifecycle_expire_delete_markers_name
}

output "lifecycle_expire_delete_markers_prefix" {
  value = var.lifecycle_expire_delete_markers_prefix
}

################################################################################

output "lifecycle_expire_objects" {
  value = var.lifecycle_expire_objects
}

output "lifecycle_expire_objects_days" {
  value = var.lifecycle_expire_objects_days
}

output "lifecycle_expire_objects_name" {
  value = var.lifecycle_expire_objects_name
}

output "lifecycle_expire_objects_prefix" {
  value = var.lifecycle_expire_objects_prefix
}

################################################################################

output "lifecycle_expire_versions" {
  value = var.lifecycle_expire_versions
}

output "lifecycle_expire_versions_days" {
  value = var.lifecycle_expire_versions_days
}

output "lifecycle_expire_versions_name" {
  value = var.lifecycle_expire_versions_name
}

output "lifecycle_expire_versions_prefix" {
  value = var.lifecycle_expire_versions_prefix
}

################################################################################

output "lifecycle_multipart_upload_clear" {
  value = var.lifecycle_multipart_upload_clear
}

output "lifecycle_multipart_upload_clear_days" {
  value = var.lifecycle_multipart_upload_clear_days
}

output "lifecycle_multipart_upload_clear_name" {
  value = var.lifecycle_multipart_upload_clear_name
}

output "lifecycle_multipart_upload_clear_prefix" {
  value = var.lifecycle_multipart_upload_clear_prefix
}

################################################################################

output "lifecycle_id" {
  value = aws_s3_bucket_lifecycle_configuration.scope.id
}

################################################################################
