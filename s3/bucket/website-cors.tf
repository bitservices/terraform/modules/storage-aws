################################################################################
# Optional Variables
################################################################################

variable "website_cors_enabled" {
  type        = bool
  default     = true
  description = "Should CORS be enabled on this S3 website? If 'false' all other options relating to CORS are ignored."
}

variable "website_cors_allow_headers" {
  type        = list(string)
  default     = ["accept", "accept-language", "content-language", "content-type", "origin"]
  description = "Specifies which headers are allowed in CORS requests."
}

variable "website_cors_allow_methods" {
  type        = list(string)
  default     = ["GET"]
  description = "Specifies which methods are allowed in CORS requests."
}

variable "website_cors_allow_origins" {
  type        = list(string)
  default     = []
  description = "Specifies which origins are allowed in CORS requests. If empty, 'website_cors_enabled' is overridden to 'false'."
}

variable "website_cors_expose_headers" {
  type        = list(string)
  default     = ["cache-control", "content-language", "content-length", "content-type", "expires", "last-modified", "pragma"]
  description = "Specifies exposed header list in the CORS response."
}

variable "website_cors_max_age_seconds" {
  type        = number
  default     = 3600
  description = "Specifies time in seconds that browser can cache the response for a preflight request."
}

################################################################################
# Locals
################################################################################

locals {
  website_cors_enabled = var.website_enabled && length(compact(var.website_cors_allow_origins)) > 0 ? var.website_cors_enabled : false
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_cors_configuration" "scope" {
  count  = local.website_cors_enabled ? 1 : 0
  bucket = aws_s3_bucket.scope.bucket

  cors_rule {
    allowed_headers = var.website_cors_allow_headers
    allowed_methods = var.website_cors_allow_methods
    allowed_origins = var.website_cors_allow_origins
    expose_headers  = var.website_cors_expose_headers
    max_age_seconds = var.website_cors_max_age_seconds
  }
}

################################################################################
# Outputs
################################################################################

output "website_cors_enabled" {
  value = local.website_cors_enabled
}

output "website_cors_allow_headers" {
  value = var.website_cors_allow_headers
}

output "website_cors_allow_methods" {
  value = var.website_cors_allow_methods
}

output "website_cors_allow_origins" {
  value = var.website_cors_allow_origins
}

output "website_cors_expose_headers" {
  value = var.website_cors_expose_headers
}

output "website_cors_max_age_seconds" {
  value = var.website_cors_max_age_seconds
}

################################################################################

output "website_cors_id" {
  value = length(aws_s3_bucket_cors_configuration.scope) == 1 ? aws_s3_bucket_cors_configuration.scope[0].id : null
}

################################################################################
