################################################################################
# Optional Variables
################################################################################

variable "versioning_enabled" {
  type        = bool
  default     = false
  description = "Enable versioning."
}

variable "versioning_suspend" {
  type        = bool
  default     = false
  description = "Should versioning be suspended for this bucket? Ignored if 'versioning_enabled' is 'false'."
}

variable "versioning_mfa_delete" {
  type        = bool
  default     = false
  description = "Enable MFA delete for either 'Change the versioning state of your bucket' or 'Permanently delete an object version'."
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_versioning" "scope" {
  count  = var.versioning_enabled ? 1 : 0
  bucket = aws_s3_bucket.scope.id

  versioning_configuration {
    status     = var.versioning_suspend ? "Suspended" : "Enabled"
    mfa_delete = var.versioning_mfa_delete ? "Enabled" : "Disabled"
  }
}

################################################################################
# Outputs
################################################################################

output "versioning_enabled" {
  value = var.versioning_enabled
}

output "versioning_suspend" {
  value = var.versioning_suspend
}

output "versioning_mfa_delete" {
  value = var.versioning_mfa_delete
}

################################################################################

output "versioning_id" {
  value = length(aws_s3_bucket_versioning.scope) == 1 ? aws_s3_bucket_versioning.scope[0].id : null
}

################################################################################
