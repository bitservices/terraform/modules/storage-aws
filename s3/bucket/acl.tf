################################################################################
# Optional Variables
################################################################################

variable "acl" {
  type        = string
  default     = "private"
  description = "The canned ACL to apply. Ignored if 'ownership_controls' is 'BucketOwnerEnforced'."
}

################################################################################
# Locals
################################################################################

locals {
  acl_enabled = var.ownership_controls != "BucketOwnerEnforced"
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_acl" "scope" {
  count  = local.acl_enabled ? 1 : 0
  acl    = var.acl
  bucket = aws_s3_bucket.scope.id

  depends_on = [
    aws_s3_bucket_ownership_controls.scope
  ]
}

################################################################################
# Outputs
################################################################################

output "acl" {
  value = var.acl
}

################################################################################

output "acl_enabled" {
  value = local.acl_enabled
}

################################################################################

output "acl_id" {
  value = length(aws_s3_bucket_acl.scope) == 1 ? aws_s3_bucket_acl.scope[0].id : null
}

################################################################################
