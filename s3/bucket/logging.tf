################################################################################
# Optional Variables
################################################################################

variable "logging_key" {
  type        = string
  default     = null
  description = "A key to prefix to stored logs in the logging S3 bucket."
}

variable "logging_class" {
  type        = string
  default     = "logging"
  description = "This forms the name of the logging S3 bucket. Unless 'logging_bucket' is set, it is prefixed to the region and 'account' (if specified)."
}

variable "logging_bucket" {
  type        = string
  default     = null
  description = "The full name of the S3 bucket to send logs to. Generated with 'logging_class', the region and 'account' (if specified) by default."
}

variable "logging_account" {
  type        = string
  default     = null
  description = "The account ID that owns 'logging_bucket'. This is a security feature. It will use the account ID of the current Terraform session by default."
}

variable "logging_enabled" {
  type        = bool
  default     = false
  description = "Should this S3 bucket send access logs to another S3 bucket?"
}

################################################################################
# Locals
################################################################################

locals {
  logging_bucket  = coalesce(var.logging_bucket, format("%s-%s%s", var.logging_class, data.aws_region.scope.name, var.account != null ? format("-%s", var.account) : ""))
  logging_account = coalesce(var.logging_account, data.aws_caller_identity.scope.account_id)
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_logging" "scope" {
  count = var.logging_enabled ? 1 : 0

  bucket                = aws_s3_bucket.scope.id
  target_bucket         = local.logging_bucket
  target_prefix         = var.logging_key
  expected_bucket_owner = local.logging_account
}

################################################################################
# Outputs
################################################################################

output "logging_class" {
  value = var.logging_class
}

output "logging_enabled" {
  value = var.logging_enabled
}

################################################################################

output "logging_id" {
  value = length(aws_s3_bucket_logging.scope) == 1 ? aws_s3_bucket_logging.scope[0].id : null
}

output "logging_key" {
  value = length(aws_s3_bucket_logging.scope) == 1 ? aws_s3_bucket_logging.scope[0].target_prefix : null
}

output "logging_bucket" {
  value = length(aws_s3_bucket_logging.scope) == 1 ? aws_s3_bucket_logging.scope[0].target_bucket : null
}

output "logging_account" {
  value = length(aws_s3_bucket_logging.scope) == 1 ? aws_s3_bucket_logging.scope[0].expected_bucket_owner : null
}

################################################################################
