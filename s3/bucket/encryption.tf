################################################################################
# Optional Variables
################################################################################

variable "encryption_key" {
  type        = string
  default     = null
  description = "The KMS master key ID used for the SSE-KMS encryption. This can only be used when you set the value of 'encryption_type' as 'aws:kms'."
}

variable "encryption_type" {
  type        = string
  default     = "none"
  description = "The server-side encryption algorithm to use or 'none' to disable encryption."
}

################################################################################
# Locals
################################################################################

locals {
  encryption_key = var.encryption_type == "aws:kms" ? var.encryption_key : null
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_server_side_encryption_configuration" "scope" {
  count  = lower(var.encryption_type) != "none" ? 1 : 0
  bucket = aws_s3_bucket.scope.bucket

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = var.encryption_type
      kms_master_key_id = local.encryption_key
    }
  }
}

################################################################################
# Outputs
################################################################################

output "encryption_key" {
  value = local.encryption_key
}

output "encryption_type" {
  value = var.encryption_type
}

################################################################################

output "encryption_id" {
  value = length(aws_s3_bucket_server_side_encryption_configuration.scope) == 1 ? aws_s3_bucket_server_side_encryption_configuration.scope[0].id : null
}

################################################################################
