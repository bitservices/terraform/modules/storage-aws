################################################################################
# Optional Variables
################################################################################

variable "ownership_controls" {
  type        = string
  default     = "BucketOwnerEnforced"
  description = "Rule for object ownership in the S3 bucket. Must be either 'BucketOwnerEnforced', 'BucketOwnerPreferred' or 'ObjectWriter'."

  validation {
    condition     = contains(["BucketOwnerEnforced", "BucketOwnerPreferred", "ObjectWriter"], var.ownership_controls)
    error_message = "S3 bucket ownership controls setting must be either 'BucketOwnerEnforced', 'BucketOwnerPreferred' or 'ObjectWriter'."
  }
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket_ownership_controls" "scope" {
  bucket = aws_s3_bucket.scope.id

  rule {
    object_ownership = var.ownership_controls
  }
}

################################################################################
# Outputs
################################################################################

output "ownership_controls" {
  value = var.ownership_controls
}

################################################################################

output "ownership_controls_id" {
  value = aws_s3_bucket_ownership_controls.scope.id
}

################################################################################
