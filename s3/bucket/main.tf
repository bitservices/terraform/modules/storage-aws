################################################################################
# Required Variables
################################################################################

variable "class" {
  type        = string
  description = "This forms the name of the S3 bucket. Unless 'name' is set, it is prefixed to the region and 'account' (if specified)."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "name" {
  type        = string
  default     = null
  description = "The full name of this S3 bucket. Generated with 'class', the region and 'account' (if specified) by default."
}

variable "account" {
  type        = string
  default     = null
  description = "The account name to be appended to the automatically generated S3 bucket name to ensure uniqueness. Ignored if not set or 'name' is specified."
}

################################################################################

variable "force_destroy" {
  type        = bool
  default     = false
  description = "If 'true' indicates all objects should be deleted from the bucket so that the bucket can be destroyed without error."
}

################################################################################
# Locals
################################################################################

locals {
  name = coalesce(var.name, format("%s-%s%s", var.class, data.aws_region.scope.name, var.account != null ? format("-%s", var.account) : ""))
}

################################################################################
# Resources
################################################################################

resource "aws_s3_bucket" "scope" {
  bucket        = local.name
  force_destroy = var.force_destroy

  tags = {
    "Name"       = local.name
    "Class"      = var.class
    "Owner"      = var.owner
    "Region"     = data.aws_region.scope.name
    "Company"    = var.company
    "Encryption" = var.encryption_type
    "Versioning" = var.versioning_enabled && var.versioning_suspend == false ? "Enabled" : "Disabled"
  }
}

################################################################################
# Outputs
################################################################################

output "class" {
  value = var.class
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "account" {
  value = var.account
}

################################################################################

output "force_destroy" {
  value = var.force_destroy
}

################################################################################

output "arn" {
  value = aws_s3_bucket.scope.arn

  depends_on = [
    aws_s3_bucket_acl.scope,
    aws_s3_bucket_logging.scope,
    aws_s3_bucket_versioning.scope,
    aws_s3_bucket_cors_configuration.scope,
    aws_s3_bucket_website_configuration.scope,
    aws_s3_bucket_lifecycle_configuration.scope,
    aws_s3_bucket_server_side_encryption_configuration.scope
  ]
}

output "name" {
  value = aws_s3_bucket.scope.id

  depends_on = [
    aws_s3_bucket_acl.scope,
    aws_s3_bucket_logging.scope,
    aws_s3_bucket_versioning.scope,
    aws_s3_bucket_cors_configuration.scope,
    aws_s3_bucket_website_configuration.scope,
    aws_s3_bucket_lifecycle_configuration.scope,
    aws_s3_bucket_server_side_encryption_configuration.scope
  ]
}

output "zone_id" {
  value = aws_s3_bucket.scope.hosted_zone_id
}

output "domain_name" {
  value = aws_s3_bucket.scope.bucket_domain_name

  depends_on = [
    aws_s3_bucket_acl.scope,
    aws_s3_bucket_logging.scope,
    aws_s3_bucket_versioning.scope,
    aws_s3_bucket_cors_configuration.scope,
    aws_s3_bucket_website_configuration.scope,
    aws_s3_bucket_lifecycle_configuration.scope,
    aws_s3_bucket_server_side_encryption_configuration.scope
  ]
}

################################################################################
