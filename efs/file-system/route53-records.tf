################################################################################
# Optional Variables
################################################################################

variable "route53_records_ttl" {
  type        = number
  default     = 300
  description = "The Time To Live of the Route53 record."
}

variable "route53_records_type" {
  type        = string
  default     = "CNAME"
  description = "The record type, must be 'CNAME'."
}

variable "route53_records_create" {
  type        = bool
  default     = true
  description = "Should DNS records be created for this Elastic File System."
}

################################################################################
# Locals
################################################################################

locals {
  route53_records_suffix = join("", data.aws_route53_zone.scope.*.name)
}

################################################################################
# Resources
################################################################################

resource "aws_route53_record" "scope" {
  count   = var.route53_records_create ? 1 : 0
  ttl     = var.route53_records_ttl
  name    = format("%s.%s", var.name, local.route53_records_suffix)
  type    = var.route53_records_type
  records = tolist([aws_efs_file_system.scope.dns_name])
  zone_id = data.aws_route53_zone.scope[0].id
}

################################################################################
# Outputs
################################################################################

output "route53_records_create" {
  value = var.route53_records_create
}

################################################################################

output "route53_records_ttl" {
  value = length(aws_route53_record.scope) == 1 ? aws_route53_record.scope[0].ttl : null
}

output "route53_records_name" {
  value = length(aws_route53_record.scope) == 1 ? aws_route53_record.scope[0].name : null
}

output "route53_records_type" {
  value = length(aws_route53_record.scope) == 1 ? aws_route53_record.scope[0].type : null
}

################################################################################
