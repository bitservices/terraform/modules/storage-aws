################################################################################
# Resources
################################################################################

resource "aws_efs_mount_target" "scope" {
  for_each        = toset(local.subnet_ids)
  subnet_id       = each.value
  file_system_id  = aws_efs_file_system.scope.id
  security_groups = concat(var.security_group_ids, data.aws_security_group.scope.*.id)
}

################################################################################
# Outputs
################################################################################

output "target_count" {
  value = length(aws_efs_mount_target.scope)
}

################################################################################

output "target_ids" {
  value = aws_efs_mount_target.scope.*.id
}

output "target_dns_names" {
  value = aws_efs_mount_target.scope.*.dns_name
}

output "target_interface_ids" {
  value = aws_efs_mount_target.scope.*.network_interface_id
}

################################################################################
