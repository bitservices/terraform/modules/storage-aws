<!----------------------------------------------------------------------------->

# efs/file-system

#### Manages [EFS] file systems and their mount targets

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/storage/aws//efs/file-system`**

--------------------------------------------------------------------------------

### Example Usage

```
module "my_efs_filesystem" {
  source  = "gitlab.com/bitservices/storage/aws//efs/file-system"
  vpc     = "sandpit01"
  name    = "foobar"
  owner   = "terraform@bitservices.io"
  company = "BITServices Ltd"
}
```

<!----------------------------------------------------------------------------->

[EFS]: https://aws.amazon.com/efs

<!----------------------------------------------------------------------------->
