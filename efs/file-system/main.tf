################################################################################
# Required Variables
################################################################################

variable "name" {
  type        = string
  description = "The full name of this Elastic File System."
}

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "performance" {
  type        = string
  default     = "generalPurpose"
  description = "The file system performance mode."
}

################################################################################

variable "encryption_key" {
  type        = string
  default     = null
  description = "The ARN of the KMS encrpyion key. Ignored if 'encryption_enabled' is 'false'."
}

variable "encryption_enabled" {
  type        = bool
  default     = true
  description = "If 'true', the file system will be encrypted."
}

################################################################################
# Locals
################################################################################

locals {
  encryption_key = var.encryption_enabled ? var.encryption_key : null
}

################################################################################
# Resources
################################################################################

resource "aws_efs_file_system" "scope" {
  encrypted        = var.encryption_enabled
  kms_key_id       = local.encryption_key
  creation_token   = var.name
  performance_mode = var.performance

  tags = {
    VPC         = var.vpc
    Name        = var.name
    Owner       = var.owner
    Region      = data.aws_region.scope.name
    Company     = var.company
    Encrypted   = var.encryption_enabled ? "Enabled" : "Disabled"
    Performance = var.performance
  }
}

################################################################################
# Outputs
################################################################################

output "name" {
  value = var.name

  depends_on = [
    aws_efs_file_system.scope,
    aws_efs_mount_target.scope
  ]
}

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "performance" {
  value = var.performance
}

################################################################################

output "encryption_key" {
  value = local.encryption_key
}

output "encryption_enabled" {
  value = var.encryption_enabled
}

################################################################################

output "id" {
  value = aws_efs_file_system.scope.id

  depends_on = [
    aws_efs_mount_target.scope
  ]
}

output "arn" {
  value = aws_efs_file_system.scope.arn

  depends_on = [
    aws_efs_mount_target.scope
  ]
}

output "size" {
  value = aws_efs_file_system.scope.size_in_bytes
}

output "tags" {
  value = aws_efs_file_system.scope.tags_all
}

output "zone_id" {
  value = aws_efs_file_system.scope.availability_zone_id
}

output "dns_name" {
  value = aws_efs_file_system.scope.dns_name

  depends_on = [
    aws_efs_mount_target.scope
  ]
}

output "account_id" {
  value = aws_efs_file_system.scope.owner_id
}

################################################################################
