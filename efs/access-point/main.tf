################################################################################
# Required Variables
################################################################################

variable "owner" {
  type        = string
  description = "The email address of the technical/business owner for this asset."
}

variable "company" {
  type        = string
  description = "The name of the company that owns this asset."
}

################################################################################
# Optional Variables
################################################################################

variable "root_mode" {
  type        = string
  default     = "0700"
  description = "The permissions in octal to set on the folder within the Elastic File System that will act as the root folder for this access point. Ignored if 'root_path' is '/'."
}

variable "root_path" {
  type        = string
  default     = "/"
  description = "The folder within the Elastic File System that will act as the root folder for this access point."

  validation {
    condition     = trimprefix("/", var.root_path) != var.root_path
    error_message = "Root path for access point must begin with '/'."
  }
}

variable "root_user" {
  type        = number
  default     = 0
  description = "POSIX user ID to set as the owner of 'root_path'. Ignored if 'root_path' is '/'."
}

variable "root_group" {
  type        = number
  default     = 0
  description = "POSIX group ID to set as the owner of 'root_path'. Ignored if 'root_path' is '/'."
}

################################################################################

variable "override_user" {
  type        = number
  default     = null
  description = "POSIX user ID to override all file operations with. Ignored unless 'override_group' is also set."
}

variable "override_group" {
  type        = number
  default     = null
  description = "POSIX group ID to override all file operations with. Ignored unless 'override_user' is also set."
}

variable "override_extra_groups" {
  type        = list(number)
  default     = null
  description = "List of secondary POSIX group IDs to override all file operations with. Ignored unless both 'override_user' and 'override_group' are also set."
}

################################################################################
# Locals
################################################################################

locals {
  root_mode             = local.root_skip ? null : var.root_mode
  root_user             = local.root_skip ? null : var.root_user
  root_skip             = trimspace(var.root_path) == "/"
  root_group            = local.root_skip ? null : var.root_group
  override_skip         = var.override_user == null || var.override_group == null
  override_user         = local.override_skip ? null : var.override_user
  override_group        = local.override_skip ? null : var.override_group
  override_extra_groups = local.override_skip ? null : var.override_extra_groups
}

################################################################################
# Resources
################################################################################

resource "aws_efs_access_point" "scope" {
  file_system_id = data.aws_efs_file_system.scope.id

  tags = {
    "Root"       = local.root_skip ? "Disabled" : "Enabled"
    "Owner"      = var.owner
    "Region"     = data.aws_region.scope.name
    "Company"    = var.company
    "Override"   = local.override_skip ? "Disabled" : "Enabled"
    "FileSystem" = var.file_system_name
  }

  dynamic "posix_user" {
    for_each = local.override_skip ? [] : [local.override_skip]

    content {
      gid            = local.override_group
      uid            = local.override_user
      secondary_gids = local.override_extra_groups
    }
  }

  dynamic "root_directory" {
    for_each = local.root_skip ? [] : [local.root_skip]

    content {
      path = var.root_path

      creation_info {
        owner_gid   = local.root_group
        owner_uid   = local.root_user
        permissions = local.root_mode
      }
    }
  }
}

################################################################################
# Outputs
################################################################################

output "owner" {
  value = var.owner
}

output "company" {
  value = var.company
}

################################################################################

output "root_mode" {
  value = local.root_mode
}

output "root_path" {
  value = var.root_path
}

output "root_user" {
  value = local.root_user
}

output "root_group" {
  value = local.root_group
}

################################################################################

output "override_user" {
  value = local.override_user
}

output "override_group" {
  value = local.override_group
}

output "override_extra_groups" {
  value = local.override_extra_groups
}

################################################################################

output "root_skip" {
  value = local.root_skip
}

output "override_skip" {
  value = local.override_skip
}

################################################################################

output "id" {
  value = aws_efs_access_point.scope.id
}

output "arn" {
  value = aws_efs_access_point.scope.arn
}

output "tags" {
  value = aws_efs_access_point.scope.tags_all
}

################################################################################
