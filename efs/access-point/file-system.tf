################################################################################
# Required Variables
################################################################################

variable "file_system_name" {
  type        = string
  description = "The full name of the Elastic File System that this access point will be for."
}

################################################################################
# Optional Variables
################################################################################

variable "file_system_id" {
  type        = string
  default     = null
  description = "The ID of the Elastic File System that this access point will be for. Must be specified if the Elastic File System is created in the same Terraform run."
}

################################################################################
# Data Sources
################################################################################

data "aws_efs_file_system" "scope" {
  file_system_id = var.file_system_id

  tags = {
    "Name" = var.file_system_name
  }
}

################################################################################
# Outputs
################################################################################

output "file_system_name" {
  value = var.file_system_name
}

################################################################################

output "file_system_id" {
  value = data.aws_efs_file_system.scope.id
}

output "file_system_arn" {
  value = data.aws_efs_file_system.scope.arn
}

output "file_system_size" {
  value = data.aws_efs_file_system.scope.size_in_bytes
}

output "file_system_tags" {
  value = data.aws_efs_file_system.scope.tags
}

output "file_system_zone" {
  value = data.aws_efs_file_system.scope.availability_zone_name
}

output "file_system_policy" {
  value = data.aws_efs_file_system.scope.lifecycle_policy
}

output "file_system_zone_id" {
  value = data.aws_efs_file_system.scope.availability_zone_id
}

output "file_system_dns_name" {
  value = data.aws_efs_file_system.scope.dns_name
}

output "file_system_performance" {
  value = data.aws_efs_file_system.scope.performance_mode
}

output "file_system_throughput_mb" {
  value = data.aws_efs_file_system.scope.provisioned_throughput_in_mibps
}

output "file_system_encryption_key" {
  value = data.aws_efs_file_system.scope.kms_key_id
}

output "file_system_throughput_type" {
  value = data.aws_efs_file_system.scope.throughput_mode
}

output "file_system_encryption_enabled" {
  value = data.aws_efs_file_system.scope.encrypted
}

################################################################################
