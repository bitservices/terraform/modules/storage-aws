<!----------------------------------------------------------------------------->

# efs/access-point

#### Manages [EFS] access points

--------------------------------------------------------------------------------

Source **`gitlab.com/bitservices/storage/aws//efs/access-point`**

--------------------------------------------------------------------------------

### Example Usage

```
variable "owner"   { default = "terraform@bitservices.io" }
variable "company" { default = "BITServices Ltd"          }

module "my_efs_filesystem" {
  source  = "gitlab.com/bitservices/storage/aws//efs/file-system"
  vpc     = "sandpit01"
  name    = "foobar"
  owner   = var.owner
  company = var.company
}

module "my_efs_access_point" {
  source           = "gitlab.com/bitservices/storage/aws//efs/access-point"
  owner            = var.owner
  company          = var.company
  file_system_id   = module.my_efs_filesystem.id
  file_system_name = module.my_efs_filesystem.name
}
```

<!----------------------------------------------------------------------------->

[EFS]: https://aws.amazon.com/efs

<!----------------------------------------------------------------------------->
