<!----------------------------------------------------------------------------->

# storage (aws)

<!----------------------------------------------------------------------------->

## Description

Provisions storage on [AWS] with services such as [S3] or
Elastic File System ([EFS]).

<!----------------------------------------------------------------------------->

## Modules

* [efs/access-point](efs/access-point/README.md) - Manage [EFS] access points.
* [efs/file-system](efs/file-system/README.md) - Manage [EFS] file systems and their mount targets.
* [s3/bucket](s3/bucket/README.md) - Manage [S3] buckets and websites.
* [s3/notification-queue](s3/notification-queue/README.md) - Manage [S3] bucket notifications that are put into [SQS] queues.
* [s3/policy](s3/policy/README.md) - Manage [S3] bucket policies.

<!----------------------------------------------------------------------------->

[S3]:  https://aws.amazon.com/s3/
[AWS]: https://aws.amazon.com/
[EFS]: https://aws.amazon.com/efs/
[SQS]: https://aws.amazon.com/sqs/

<!----------------------------------------------------------------------------->
